import socket
import logging

from _thread import *
import threading

HOST = "127.0.0.1"
PORT = 10001

SERVER_KEY = 54621
CLIENT_KEY = 45328

SERVER_MOVE = "102 MOVE\a\b"
SERVER_TURN_LEFT = "103 TURN LEFT\a\b"
SERVER_TURN_RIGHT = "104 TURN RIGHT\a\b"
SERVER_PICK_UP = "105 GET MESSAGE\a\b"
SERVER_LOGOUT = "106 LOGOUT\a\b"
SERVER_OK = "200 OK\a\b"
SERVER_LOGIN_FAILED = "300 LOGIN FAILED\a\b"
SERVER_SYNTAX_ERROR = "301 SYNTAX ERROR\a\b"
SERVER_LOGIC_ERROR = "302 LOGIC ERROR\a\b"

CLIENT_RECHARGING = "RECHARGING\a\b"
CLIENT_FULL_POWER = "FULL POWER\a\b"

CLIENT_USERNAME_MAX_LENGTH = 12
CLIENT_CONFIRMATION_MAX_LENGTH = 7
CLIENT_OK_MAX_LENGTH = 12
CLIENT_RECHARGING_MAX_LENGTH = 12
CLIENT_FULL_POWER_MAX_LENGTH = 12
CLIENT_MESSAGE_MAX_LENGTH = 100

TIMEOUT = 1
TIMEOUT_RECHARGING = 5

logging.basicConfig(level=logging.DEBUG)


def getMessage(conn, maxLen, tmpBuffer, buffer):
    if (len(buffer) == 0):
        message, tmpBuffer, buffer = messageFragmentation(conn, maxLen, tmpBuffer, buffer)
    else:
        # print("test2")
        print("buffer:", buffer)
        print("bufferlen:", len(buffer))
        print("tmpBuffer:", tmpBuffer)
        print('\n')
        message = buffer[0] + "\x07\x08"
        buffer = buffer[1:]
        message = message.encode()

    return message, tmpBuffer, buffer


def messageFragmentation(conn, maxLen, tmpBuffer, buffer):
    tmpMessage = ''

    while True:
        tmpMessage = conn.recv(1024)
        print("tmpMessage:", tmpMessage)
        # print("Received message:", tmpMessage)

        # if tmpMessage == '':
        #     raise Exception("zero size exception")

        concatenatedMessage = ''
        # splittedMessage = tmpMessage.decode().split("\x07\x08")

        if tmpBuffer != '':
            concatenatedMessage = tmpBuffer + tmpMessage.decode()
            tmpBuffer = ''
        else:
            concatenatedMessage = tmpMessage.decode()

        logging.info(concatenatedMessage)

        if len(concatenatedMessage) == 0:
            raise Exception("zero size exception")

        splittedMessage = concatenatedMessage.split("\x07\x08")

        if splittedMessage[-1] == '':
            # concatenated message is ended by \a\b
            # print("test")

            splittedMessage = splittedMessage[:-1]

            # if len(splittedMessage) == 0:
            #     raise Exception("zero size exception")

            if len(splittedMessage) > 1:
                buffer = splittedMessage[1:]

            # buffer = splittedMessage[1:]

            # print("TmpMessage: ", tmpMessage)
            # print("Concatenated message:", concatenatedMessage)
            # print("Splitted message:", splittedMessage)

            # logging.info(splittedMessage)
            message = splittedMessage[0] + "\x07\x08"
            # print("Message is full1:", message.encode())
            # print("\n")

            return message.encode(), tmpBuffer, buffer
        else:
            # concatenated message is not ended by \a\b -> last message is not full
            # splittedMessage[-1] is

            if len(splittedMessage) > 1:
                # in slpit message exists at least one full message
                buffer = splittedMessage[1:-1]
                tmpBuffer = splittedMessage[-1]
                message = splittedMessage[0] + "\x07\x08"

                # print("Message contains full parts, message:", tmpMessage)
                # print("buffer:", buffer)
                # print("tmpBuffer:", tmpBuffer)

                return message.encode(), tmpBuffer, buffer
            else:
                tmpBuffer = splittedMessage[0]

                if len(tmpBuffer) >= maxLen:
                    # print("Messager is too long:", tmpBuffer)
                    return tmpBuffer.encode(), tmpBuffer, buffer
                # print("Message DOESN'T contains full parts, message:", tmpMessage)
                # print("buffer:", buffer)
                # print("tmpBuffer:", tmpBuffer)
                continue


def countConfirmCode(name, key):
    sum = 0

    for i in range(len(name) - 2):
        sum += ord(name[i])
        # print(ord(name[i]))

    # print(sum)
    hash = (sum * 1000) % 65536
    confirmCode = (hash + key) % 65536
    confirmCode = str(confirmCode) + "\x07\x08"

    return confirmCode


def isValid(message, maxLen, type=''):
    # print("message to be checked:", message)

    if len(message) > maxLen:
        # logging.info("LEN:", len(message))
        # logging.info("Finished here")
        return False

    if len(message) < 2:
        # print("not valid message2:", message)
        # logging.info("Finished here2")
        return False
    else:
        if message[-1] != 8 or message[-2] != 7:
            # logging.info("Finished here3")
            # print("should be", ord("\b"), ord("\a"))
            # print(ord(message[-1]), ord(message[-2]))
            # print("not valid message2:", message)
            return False

    if type == "CLIENT_OK":

        message = message.decode()

        if len((message[:-2]).split(" ")) != 3 or ((message[:-2]).split(" "))[0] != "OK":
            # logging.info("Finished here4")
            return False

        x1 = ((message[:-2]).split(" "))[1]
        y1 = ((message[:-2]).split(" "))[2]

        if not x1.lstrip('-').isdigit() or not y1.lstrip('-').isdigit():
            # logging.info("Non digit: ", x1, y1)
            return False

    # print("test0")
    return True


def robotRecharging(conn, tmpBuffer, buffer):
    conn.settimeout(TIMEOUT_RECHARGING)

    clientMessage, tmpBuffer, buffer = getMessage(conn, CLIENT_RECHARGING_MAX_LENGTH, tmpBuffer, buffer)

    conn.settimeout(TIMEOUT)

    if clientMessage != CLIENT_FULL_POWER.encode():
        # conn.sendall(SERVER_SYNTAX_ERROR.encode())
        # logging.warning("NOT recharged")

        conn.sendall(SERVER_LOGIC_ERROR.encode())
        conn.close()
        return False, tmpBuffer, buffer

    # logging.info("recharged")

    return True, tmpBuffer, buffer


def findDirection(x1, y1, x2, y2):
    # 0 stands for UP; 1 for RIGHT; 2 dor DOWN; 3 for LEFT;

    direction = -1
    if x2 > x1 and y1 == y2:
        direction = 1
    elif x2 < x1 and y1 == y2:
        direction = 3
    elif y2 > y1 and x1 == x2:
        direction = 0
    elif y2 < y1 and x1 == x2:
        direction = 2
    else:
        print("strange direction:", direction)

    return direction


def changeDirection(finalDirection, conn, direction, tmpBuffer, buffer):
    # global direction

    initDirection = direction

    while True:
        # print("direction: ",initDirection)
        if initDirection == finalDirection:
            break

        if finalDirection - initDirection == 1:
            conn.sendall(SERVER_TURN_RIGHT.encode())
            initDirection = (initDirection + 1) % 4
            print("turn1")
        elif finalDirection - initDirection == -3:
            conn.sendall(SERVER_TURN_RIGHT.encode())
            initDirection = (initDirection + 1) % 4
            print("turn2")
        elif finalDirection - initDirection == -1:
            conn.sendall(SERVER_TURN_LEFT.encode())
            initDirection = (initDirection - 1) % 4
            print("turn3")
        elif finalDirection - initDirection == 3:
            conn.sendall(SERVER_TURN_LEFT.encode())
            initDirection = (initDirection - 1) % 4
            print("turn4")
        elif finalDirection - initDirection == -2 or finalDirection - initDirection == 2:
            conn.sendall(SERVER_TURN_LEFT.encode())
            initDirection = (initDirection - 1) % 4
            print(initDirection)
            print("turn5")

        message, tmpBuffer, buffer = getMessage(conn, CLIENT_RECHARGING_MAX_LENGTH, tmpBuffer, buffer)

        if message == CLIENT_RECHARGING.encode():
            success, tmpBuffer, buffer = robotRecharging(conn, tmpBuffer, buffer)
            if not success:
                conn.sendall(SERVER_LOGIC_ERROR.encode())
                conn.close()
                raise Exception("Logic error exception")
                # return -1, tmpBuffer, buffer

            message, tmpBuffer, buffer = getMessage(conn, CLIENT_OK_MAX_LENGTH, tmpBuffer, buffer)

        if not isValid(message, CLIENT_OK_MAX_LENGTH):
            conn.sendall(SERVER_SYNTAX_ERROR.encode())
            conn.close()
            raise Exception("Syntax error exception")

    # print("changed direction:", finalDirection)

    # initiDirection now is equal to finalDirection
    direction = initDirection

    return direction, tmpBuffer, buffer


def moveTo(x, y, xDest, yDest, conn, direction, tmpBuffer, buffer):
    if x == xDest and y == yDest:
        return direction, tmpBuffer, buffer

    x = int(x)
    y = int(y)

    if xDest > x:
        print("test1")
        direction, tmpBuffer, buffer = changeDirection(1, conn, direction, tmpBuffer, buffer)
    elif xDest < x:
        print("test2")
        direction, tmpBuffer, buffer = changeDirection(3, conn, direction, tmpBuffer, buffer)

    # moving on X axis
    # print("xDest:", xDest)
    while True:

        # print("xDest = %d ; actualx = %d " % (xDest, x))

        if x == xDest:
            break
        else:
            conn.sendall(SERVER_MOVE.encode())

            # message = conn.recv(1024)
            # message = messageFragmentation(message, conn)
            message, tmpBuffer, buffer = getMessage(conn, CLIENT_OK_MAX_LENGTH, tmpBuffer, buffer)

            # logging.warning(message)

            print("test1")
            if message == CLIENT_RECHARGING.encode():
                success, tmpBuffer, buffer = robotRecharging(conn, tmpBuffer, buffer)
                if not success:
                    conn.sendall(SERVER_LOGIC_ERROR.encode())
                    conn.close()
                    raise Exception("Logic error exception")
                    # return -1, tmpBuffer, buffer
                message, tmpBuffer, buffer = getMessage(conn, CLIENT_OK_MAX_LENGTH, tmpBuffer, buffer)

            if not isValid(message, CLIENT_OK_MAX_LENGTH, "CLIENT_OK"):
                conn.sendall(SERVER_SYNTAX_ERROR.encode())
                conn.close()
                raise Exception("Syntax error exception")
            message = message.decode()

            xNew = ((message.split("\x07\x08")[0]).split(" "))[1]
            yNew = ((message.split("\x07\x08")[0]).split(" "))[2]
            xNew = int(xNew)
            yNew = int(yNew)

            direction = findDirection(x, y, xNew, yNew)

            x = xNew
            y = yNew

            # print("new coodinates:", xNew, yNew)

            # print("actual location", x, y)

    # print("here")
    # print("testM0")

    if yDest > y:
        # print("test3")
        direction, tmpBuffer, buffer = changeDirection(0, conn, direction, tmpBuffer, buffer)

    elif yDest < y:
        # print("test4")
        direction, tmpBuffer, buffer = changeDirection(2, conn, direction, tmpBuffer, buffer)

    # print("testM")
    # moving on Y axis
    while True:

        # print("yDest = %d ; actualy = %d " % (yDest, y))
        if y == yDest:
            print("break1")
            break
        else:
            conn.sendall(SERVER_MOVE.encode())

            # message = conn.recv(1024)
            # message = messageFragmentation(message, conn)

            print("test2")
            message, tmpBuffer, buffer = getMessage(conn, CLIENT_OK_MAX_LENGTH, tmpBuffer, buffer)

            if message == CLIENT_RECHARGING.encode():
                success, tmpBuffer, buffer = robotRecharging(conn, tmpBuffer, buffer)

                if not success or not isValid(message, CLIENT_RECHARGING_MAX_LENGTH):
                    conn.sendall(SERVER_LOGIC_ERROR.encode())
                    # conn.close()
                    return -1, tmpBuffer, buffer
                message, tmpBuffer, buffer = getMessage(conn, CLIENT_OK_MAX_LENGTH, tmpBuffer, buffer)

            if not isValid(message, CLIENT_OK_MAX_LENGTH, "CLIENT_OK"):
                print("break2")
                return -1, tmpBuffer, buffer
            message = message.decode()

            xNew = ((message.split("\x07")[0]).split(" "))[1]
            yNew = ((message.split("\x07")[0]).split(" "))[2]
            xNew = int(xNew)
            yNew = int(yNew)

            direction = findDirection(x, y, xNew, yNew)

            x = xNew
            y = yNew

            # print("new coodinates:", xNew, yNew)
            # print(x, y)

    # print("final pos:", x, y)

    return direction, tmpBuffer, buffer


def robotMovement(conn, tmpBuffer, buffer):
    direction = None

    x1 = None
    y2 = None

    x2 = None
    y2 = None

    # first move
    conn.sendall(SERVER_MOVE.encode())

    # clientMessage = conn.recv(1024)
    #
    #
    #
    # clientMessage = messageFragmentation(clientMessage, conn)
    clientMessage, tmpBuffer, buffer = getMessage(conn, CLIENT_OK_MAX_LENGTH, tmpBuffer, buffer)

    if clientMessage == CLIENT_RECHARGING.encode():
        success, tmpBuffer, buffer = robotRecharging(conn, tmpBuffer, buffer)
        if not success:
            conn.sendall(SERVER_LOGIC_ERROR.encode())
            return
        clientMessage, tmpBuffer, buffer = getMessage(conn, CLIENT_OK_MAX_LENGTH, tmpBuffer, buffer)

    if not isValid(clientMessage, CLIENT_OK_MAX_LENGTH, "CLIENT_OK"):
        conn.sendall(SERVER_SYNTAX_ERROR.encode())
        return

    clientMessage = clientMessage.decode()
    print("Buffer:", buffer)

    print("Client message:", clientMessage)
    x1 = int(((clientMessage.split("\x07")[0]).split(" "))[1])
    y1 = int(((clientMessage.split("\x07")[0]).split(" "))[2])

    x2 = x1
    y2 = y1

    # second move

    while True:
        conn.sendall(SERVER_MOVE.encode())

        clientMessage, tmpBuffer, buffer = getMessage(conn, CLIENT_OK_MAX_LENGTH, tmpBuffer, buffer)

        if clientMessage == CLIENT_RECHARGING.encode():
            success, tmpBuffer, buffer = robotRecharging(conn, tmpBuffer, buffer)
            if not success:
                conn.sendall(SERVER_LOGIC_ERROR.encode())
                return
            clientMessage, tmpBuffer, buffer = getMessage(conn, CLIENT_OK_MAX_LENGTH, tmpBuffer, buffer)

        if not isValid(clientMessage, CLIENT_OK_MAX_LENGTH, "CLIENT_OK"):
            conn.sendall(SERVER_SYNTAX_ERROR.encode())
            return
        else:
            clientMessage = clientMessage.decode()
            x2 = int(((clientMessage.split("\x07\x08")[0]).split(" "))[1])
            y2 = int(((clientMessage.split("\x07\x08")[0]).split(" "))[2])
        if x1 == x2 and y1 == y2:
            continue
        else:
            break

    direction = findDirection(x1, y1, x2, y2)

    if direction == -1:
        conn.sendall(SERVER_LOGIC_ERROR.encode())
        return

    x1 = x2
    y1 = y2

    for i in range(-2, 3, 1):
        if i % 2 == 0:
            step = 1
            xInit = -2
            xFinal = 3
        else:
            step = -1
            xInit = 2
            xFinal = -3
        for j in range(xInit, xFinal, step):
            # print("looking for message", j, i)

            direction, tmpBuffer, buffer = moveTo(x1, y1, j, i, conn, direction, tmpBuffer, buffer)
            print("coordinates:", j, i)
            x1 = j
            y1 = i

            # print("testU")
            conn.sendall(SERVER_PICK_UP.encode())

            message, tmpBuffer, buffer = getMessage(conn, CLIENT_MESSAGE_MAX_LENGTH, tmpBuffer, buffer)

            if message == CLIENT_RECHARGING.encode():
                success, tmpBuffer, buffer = robotRecharging(conn, tmpBuffer, buffer)

                if not success or not isValid(message, CLIENT_RECHARGING_MAX_LENGTH):
                    conn.sendall(SERVER_LOGIC_ERROR.encode())
                    # logging.info("no success")
                    return
                # logging.info("success")
                message, tmpBuffer, buffer = getMessage(conn, CLIENT_USERNAME_MAX_LENGTH, tmpBuffer, buffer)
                print(message)

            if not isValid(message, CLIENT_MESSAGE_MAX_LENGTH):
                conn.sendall(SERVER_SYNTAX_ERROR.encode())
                # conn.close()
                return

            if len(message) == 2:
                continue
            else:
                conn.sendall(SERVER_LOGOUT.encode())
            return


def communicate(conn):
    buffer = []
    tmpBuffer = ''

    try:
        conn.settimeout(TIMEOUT)

        # receving username
        data, tmpBuffer, buffer = getMessage(conn, CLIENT_USERNAME_MAX_LENGTH, tmpBuffer, buffer)

        if data == CLIENT_RECHARGING.encode():
            success, tmpBuffer, buffer = robotRecharging(conn, tmpBuffer, buffer)

            if not success or not isValid(data, CLIENT_RECHARGING_MAX_LENGTH):
                conn.sendall(SERVER_LOGIC_ERROR.encode())
                # conn.close()
                return
            data, tmpBuffer, buffer = getMessage(conn, CLIENT_USERNAME_MAX_LENGTH, tmpBuffer, buffer)

        if not isValid(data, CLIENT_RECHARGING_MAX_LENGTH):
            conn.sendall(SERVER_SYNTAX_ERROR.encode())
            return

        data.decode()
        # print("test")

        # server count confirmation code and send it to client
        serverConfirmCode = countConfirmCode(data.decode(), SERVER_KEY)

        conn.sendall(serverConfirmCode.encode())

        # receiving client confirmation code

        clientReceivedCode = None

        # receiving client confirmantion
        clientReceivedCode, tmpBuffer, buffer = getMessage(conn, 12, tmpBuffer, buffer)

        if clientReceivedCode == CLIENT_RECHARGING.encode():
            success, tmpBuffer, buffer = robotRecharging(conn, tmpBuffer, buffer)

            if not success or not isValid(clientReceivedCode, CLIENT_RECHARGING_MAX_LENGTH):
                conn.sendall(SERVER_LOGIC_ERROR.encode())
                # logging.info("no success")
                return
            # logging.info("success")
            clientReceivedCode, tmpBuffer, buffer = getMessage(conn, CLIENT_USERNAME_MAX_LENGTH, tmpBuffer, buffer)
            print(clientReceivedCode)

        if not isValid(clientReceivedCode, CLIENT_CONFIRMATION_MAX_LENGTH):
            conn.sendall(SERVER_SYNTAX_ERROR.encode())
            # conn.close()
            return

        clientReceivedCode = clientReceivedCode.decode()
        clientConfirmCode = countConfirmCode(data.decode(), CLIENT_KEY)
        # print(clientConfirmCode)

        for i in range(len(clientReceivedCode) - 2):
            if ord(clientReceivedCode[i]) < 48 or ord(clientReceivedCode[i]) > 57:
                conn.sendall(SERVER_SYNTAX_ERROR.encode())
                # conn.close()
                return

        # client clientConfirmCode is code counted on the server side
        if clientReceivedCode == clientConfirmCode:
            conn.sendall(SERVER_OK.encode())
        else:

            conn.sendall(SERVER_LOGIN_FAILED.encode())
            # conn.close()
            return

        robotMovement(conn, tmpBuffer, buffer)

    finally:
        logging.warning("Closing connection")
        conn.close()


def main():
    threads = []

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as mysocket:
        mysocket.bind((HOST, PORT))

        mysocket.listen(1)

        for i in range(100):
            conn, addr = mysocket.accept()

            # conn.settimeout(TIMEOUT)

            # thread = threading.Thread(target=communicate, args=(conn, ))
            start_new_thread(communicate, (conn,))

        # conn, addr = mysocket.accept()
        #
        # communicate(conn)

        # mysocket.close()


main()
